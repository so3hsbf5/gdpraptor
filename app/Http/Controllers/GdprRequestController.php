<?php

namespace App\Http\Controllers;

use App\Domain\GdprObject;
use App\Models\GdprCase;
use App\Models\GdprRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Validator;

class GdprRequestController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $now = Carbon::today()->format('Y-m-d');

        $running = GdprRequest::whereIn('state', ['SENT', 'PENDING'])->with('gdprCase')->get();
        $expired = GdprRequest::whereDate('expires_at', '<', $now)->with('gdprCase')->get();
        $all = GdprRequest::all();

        return view('pages.requests.index')
            ->with('runningRequests', $running)
            ->with('expiredRequests', $expired)
            ->with('allRequests', $all)
        ;
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create(Request $request)
    {
        $relatedCase = null;

        if ($request->has('case')) {
            $relatedCase = GdprCase::findOrFail($request->get('case'));
        }

        return view('pages.requests.create', compact('relatedCase'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            "case_id" => "required|exists:gdpr_cases,id",
            "objects" => "required|array",
            "notes"   => "present",
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withInput()->withErrors($validator);
        }

        $objects = [];
        foreach($request->get('objects') as $object => $v) {
            $objects[] = GdprObject::tryFrom($object);
        }

        $gdprRequest = GdprRequest::create([
            "gdpr_case_id" => $request->get("case_id"),
            "objects"      => $objects,
            "notes"        => $request->get("notes"),
        ]);

        return redirect()->route('requests.show', $gdprRequest)->with("success", "Request has been created");
    }

    /**
     * Display the specified resource.
     */
    public function show(GdprRequest $gdprRequest)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(GdprRequest $gdprRequest)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, GdprRequest $gdprRequest)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(GdprRequest $gdprRequest)
    {
        //
    }
}
