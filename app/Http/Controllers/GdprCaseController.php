<?php

namespace App\Http\Controllers;

use App\Models\Company;
use App\Models\GdprCase;
use App\Models\Proof;
use App\Models\Transition;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class GdprCaseController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $now = Carbon::today()->format('Y-m-d');

        $runningCases = GdprCase::whereHas('gdprRequests', function (Builder $query) {
            $query->whereIn('state', ['SENT', 'PENDING']);
        })->whereHas('complaints', function (Builder $query) {
            $query->whereIn('state', ['SENT', 'PENDING']);
        })->get();

        $expiredCases = GdprCase::whereHas('gdprRequests', function (Builder $query) use ($now) {
            $query->whereDate('expires_at', '<', $now);
        })->with('complaints')->get();

        $emptyCases = GdprCase::doesntHave('gdprRequests')->doesntHave('complaints')->get();

        $otherCases = GdprCase::with('complaints')
                              ->with('gdprRequests')
                              ->get()
                              ->diff($runningCases)
                              ->diff($expiredCases)
                              ->diff($emptyCases)
        ;

        $totalCases = count($runningCases) + count($expiredCases) + count($emptyCases) + count($otherCases);

        return view('pages.cases.index')
            ->with('runningCases', $runningCases)
            ->with('expiredCases', $expiredCases)
            ->with('emptyCases', $emptyCases)
            ->with('otherCases', $otherCases)
            ->with('totalCases', $totalCases)
        ;
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('pages.cases.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            "company_website"    => "required|regex:/.+\..+/",
            "company_name"       => "required",
            "company_email"      => "required|email",
            "company_logo"       => "sometimes|image",
            "company_parameters" => "present",
            "object"             => "required",
            "proof"              => "required|file",
            "notes"              => "present",
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withInput()->withErrors($validator);
        }
        $logo = null;
        if ($request->hasFile('company_logo')) {
            $logo = $request->file('company_logo')->store('companies', 'public');
        }

        $parameters = [];
        if (!empty($request->get('company_parameters'))) {
            $parametersRows = explode("\n", $request->get('company_parameters'));
            foreach ($parametersRows as $row) {
                [$key, $value] = explode(",", $row);
                $parameters[Str::slug($key)] = trim($value);
            }
        }

        DB::transaction(static function () use ($request, $logo, $parameters) {
            $company = Company::updateOrCreate(
                ["website" => $request->get("company_website")],
                [
                    "name"       => $request->get("company_name"),
                    "email"      => $request->get("company_email"),
                    "logo"       => $logo,
                    "parameters" => $parameters,
                ]
            );

            $case = GdprCase::create([
                "company_id" => $company->id,
                "user_id"    => Auth::getUser()?->id,
                "object"     => $request->get("object"),
                "notes"      => $request->get("notes"),
            ]);

            $path = $request->file('proof')?->store('files', 'public');
            $proof = new Proof([
                "path" => $path,
                "mime" => $request->file('proof')?->getMimeType(),
            ]);
            $case->proof()->save($proof);
        });

        return redirect()->route('cases.index')->with("success", "Case has been created");
    }

    /**
     * Display the specified resource.
     */
    public function show($caseId)
    {
        $case = GdprCase::with('complaints', 'gdprRequests', 'transitions')->findOrFail($caseId);

        $transitions = new Collection($case->transitions);

        foreach ($case->gdprRequests as $gdprRequest) {
            $transitions = $transitions->merge($gdprRequest->transitions);
        }
        foreach ($case->complaints as $complaint) {
            $transitions = $transitions->merge($complaint->transitions);
        }

        $transitionsSorted = $transitions->sort(fn(Transition $a, Transition $b) => $a->created_at < $b->created_at);
        return view('pages.cases.show', compact('case'))->with('transitions', $transitionsSorted);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit($caseId)
    {
        $case = GdprCase::findOrFail($caseId);

        return view('pages.cases.edit', compact('case'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, $caseId)
    {
        $validator = Validator::make($request->all(), [
            "object" => "required",
            "notes"  => "present",
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withInput()->withErrors($validator);
        }

        $case = GdprCase::findOrFail($caseId);
        $case->object = $request->get('object');
        $case->notes = $request->get('notes');
        $case->save();

        return redirect()->route('cases.show', ['case' => $caseId])->with('success', 'Case has been updated');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(GdprCase $gdprCase)
    {
        //
    }
}
