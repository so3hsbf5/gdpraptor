<?php

namespace App\Domain;

enum GdprObject: string
{
    private const TRANSLATION_PREFIX = "gdpr.";

    case PURPOSE_LIMITATION = "purpose_limitation";
    case MINIMIZATION = "minimization";
    case ACCURACY = "accuracy";
    case STORAGE_LIMITATION = "storage_limitation";
    case CONSENT = "consent";
    case ACCESS = "access";
    case RECTIFICATION = "rectification";
    case ERASURE = "erasure";
    case RESTRICTION = "restriction";
    case PORTABILITY = "portability";
    case OBJECTION = "objection";
    case AUTOMATED = "automated";

    public function getName(): string
    {
        return __(self::TRANSLATION_PREFIX . strtoupper($this->value) . "_NAME");
    }

    public function getDescription(): string
    {
        return __(self::TRANSLATION_PREFIX . strtoupper($this->value) . "_DESCRIPTION");
    }
}
