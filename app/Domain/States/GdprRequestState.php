<?php

namespace App\Domain\States;

use App\Domain\States\State\Draft;
use App\Domain\States\State\Ko;
use App\Domain\States\State\Ok;
use App\Domain\States\State\Pending;
use App\Domain\States\State\Sent;
use Spatie\ModelStates\StateConfig;

abstract class GdprRequestState extends ColorState
{
    abstract public function color(): string;

    public static function config(): StateConfig
    {
        return parent::config()
            ->allowTransitions([
                [Draft::class, Sent::class],
                [Sent::class, Pending::class],
                [Pending::class, Ok::class],
                [Pending::class, Ko::class],
            ])
        ;
    }
}
