<?php

namespace App\Domain\States;

use App\Domain\States\State\Creation;
use App\Domain\States\State\Draft;
use App\Domain\States\State\Ko;
use App\Domain\States\State\Ok;
use App\Domain\States\State\Pending;
use App\Domain\States\State\Sent;
use Spatie\ModelStates\State;
use Spatie\ModelStates\StateConfig;

abstract class ColorState extends State
{
    abstract public function color(): string;

    public static function config(): StateConfig
    {
        return parent::config()->default(Draft::class)
                     ->registerState(Draft::class)
                     ->registerState(Sent::class)
                     ->registerState(Pending::class)
                     ->registerState(Ok::class)
                     ->registerState(Ko::class)
                     ->registerState(Creation::class)
        ;
    }
}
