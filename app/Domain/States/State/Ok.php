<?php

namespace App\Domain\States\State;

use App\Domain\States\GdprRequestState;

class Ok extends GdprRequestState
{
    public static string $name = 'OK';

    public function color(): string
    {
        return "success";
    }
}
