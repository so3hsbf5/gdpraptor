<?php

namespace App\Domain\States\State;

use App\Domain\States\GdprRequestState;

class Draft extends GdprRequestState
{
    public static string $name = 'DRAFT';

    public function color(): string
    {
        return "dark";
    }
}
