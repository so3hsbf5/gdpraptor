<?php

namespace App\Domain\States\State;

use App\Domain\States\GdprRequestState;

class Ko extends GdprRequestState
{
    public static string $name = 'KO';

    public function color(): string
    {
        return "danger";
    }
}
