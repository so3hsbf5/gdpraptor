<?php

namespace App\Domain\States\State;

use App\Domain\States\GdprRequestState;

class Pending extends GdprRequestState
{
    public static string $name = 'PENDING';

    public function color(): string
    {
        return "primary";
    }
}
