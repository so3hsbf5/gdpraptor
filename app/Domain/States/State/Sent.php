<?php

namespace App\Domain\States\State;

use App\Domain\States\GdprRequestState;

class Sent extends GdprRequestState
{

    public function color(): string
    {
        return "info";
    }
}
