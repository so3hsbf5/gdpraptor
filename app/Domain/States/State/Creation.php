<?php

namespace App\Domain\States\State;

use App\Domain\States\GdprRequestState;

class Creation extends GdprRequestState
{
    public static string $name = "CREATION";

    public function color(): string
    {
        return "info";
    }
}
