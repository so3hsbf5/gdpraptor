<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\MorphMany;

class Complaint extends Model
{
    use HasFactory;

    public function gdprCase(): BelongsTo
    {
        return $this->belongsTo(GdprCase::class);
    }

    public function transitions(): MorphMany
    {
        return $this->morphMany(Transition::class, 'transitionnable');
    }
}
