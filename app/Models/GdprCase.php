<?php

namespace App\Models;

use App\Models\Scopes\CurrentUserScope;
use GrahamCampbell\Markdown\Facades\Markdown;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use Illuminate\Database\Eloquent\Relations\MorphOne;
use Illuminate\Database\Eloquent\SoftDeletes;

class GdprCase extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = ['company_id', 'user_id', 'object', 'notes'];

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function company(): BelongsTo
    {
        return $this->belongsTo(Company::class);
    }

    public function gdprRequests(): HasMany
    {
        return $this->hasMany(GdprRequest::class);
    }

    public function complaints(): HasMany
    {
        return $this->hasMany(Complaint::class);
    }

    public function proof(): MorphOne
    {
        return $this->morphOne(Proof::class, 'proofable');
    }

    public function transitions(): MorphMany
    {
        return $this->morphMany(Transition::class, 'transitionnable');
    }


    protected static function booted(): void
    {
        static::addGlobalScope(new CurrentUserScope());

        static::created(function (GdprCase $case) {
            $transition = new Transition([
                "to" => "CREATION",
                "description" => "Creation of the case",
            ]);
            $case->transitions()->save($transition);
        });
    }

    public function notesFormatted(): Attribute
    {
        return Attribute::make(
            get: fn (mixed $value, $attributes) => $attributes['notes'] ? Markdown::convert($attributes['notes'])->getContent() : null
        );
    }
}
