<?php

namespace App\Models\Scopes;

use App\Models\GdprCase;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Scope;
use Illuminate\Support\Facades\Auth;

class CurrentUserScope implements Scope
{
    /**
     * Apply the scope to a given Eloquent query builder.
     */
    public function apply(Builder $builder, Model $model): void
    {
        if (get_class($model) === GdprCase::class) {
            $builder->where('user_id', Auth::getUser()->id);
        } else {
            $builder->whereHas('gdprCase', function (Builder $builder) {
                $builder->where('user_id', Auth::getUser()->id);
            });
        }

    }
}
