<?php

namespace App\Models;

use App\Domain\States\ColorState;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphTo;

class Transition extends Model
{
    use HasFactory;

    protected $fillable = ['from', 'to', 'description'];
    protected $casts    = [
        'created_at' => 'datetime:d/m/Y',
        'from'       => ColorState::class,
        'to'         => ColorState::class,
    ];

    public function transitionnable(): MorphTo
    {
        return $this->morphTo();
    }

    public function icon()
    {
        return match ($this->transitionnable_type) {
            GdprCase::class => "circle",
            GdprRequest::class => "square",
            Complaint::class => "triangle",
            default => "circle",
        };
    }
}
