<?php

namespace App\Models;

use App\Domain\GdprObject;
use App\Domain\States\GdprRequestState;
use App\Domain\States\State\Draft;
use App\Models\Scopes\CurrentUserScope;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Casts\Json;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use Illuminate\Support\Str;
use Spatie\ModelStates\HasStates;

class GdprRequest extends Model
{
    use HasFactory, HasStates;

    protected $fillable = ['gdpr_case_id', 'prolonged', 'created_at', 'expire_at', 'state', 'objects', 'notes'];

    protected $casts = [
        'state'   => GdprRequestState::class,
        'objects' => 'array',
    ];

    public function gdprCase(): BelongsTo
    {
        return $this->belongsTo(GdprCase::class);
    }

    public function transitions(): MorphMany
    {
        return $this->morphMany(Transition::class, 'transitionnable');
    }

    private function getExpirationDate(): ?string
    {
        $sendDate = Carbon::make($this->sentAt);
        $expiringDate = $sendDate->addMonthsNoOverflow($this->prolonged ? 3:1);

        return $expiringDate->format("Y-m-d");
    }

    public function objects(): Attribute
    {
        return Attribute::make(
            get: function (string $objects) {
                $decoded = Json::decode($objects);
                return array_map(fn($object) => GdprObject::tryFrom($object), $decoded);
            },
        );
    }

    public function objectsAsString(): Attribute
    {
        return Attribute::make(
            get: function () {
                return implode(", ", array_map(fn(GdprObject $object) => $object->getName(), $this->objects));
            },
        );
    }

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new CurrentUserScope());
        static::creating(function (GdprRequest $request) {
            $request->expires_at = $request->sentAt ? $request->getExpirationDate():null;
        });

        static::created(function (GdprRequest $gdprRequest) {
            $transition = new Transition([
                "to"          => Draft::class,
                "description" => "Creation of the request #" . $gdprRequest->id,
            ]);
            $gdprRequest->transitions()->save($transition);
        });
    }
}
