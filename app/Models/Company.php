<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Company extends Model
{
    use HasFactory;

    protected $fillable = ['name', 'website', 'email', 'parameters', 'logo'];

    protected $casts = [
        'parameters' => 'array'
    ];

    public function gdprCases(): HasMany
    {
        return $this->hasMany(GdprCase::class);
    }

    public function logo(): Attribute
    {
        return Attribute::make(
            get: function (mixed $value, array $attributes) {
                return is_null($value) ?
                    "https://api.dicebear.com/6.x/initials/svg?seed=" . $attributes['name']
                    : $value;
            },
        );
    }
}
