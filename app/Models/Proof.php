<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphTo;

class Proof extends Model
{
    use HasFactory;

    protected $fillable = ['path', 'mime', 'description'];

    public function proofable(): MorphTo
    {
        return $this->morphTo();
    }
}
