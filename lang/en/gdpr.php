<?php
return [
    "PURPOSE_LIMITATION_NAME"        => "Purpose Limitation",
    "PURPOSE_LIMITATION_DESCRIPTION" => "Article 5.1.b - Personal data should be collected for specified, explicit, and legitimate purposes.",
    "MINIMIZATION_NAME"              => "Data Minimization",
    "MINIMIZATION_DESCRIPTION"       => "Article 5.1.c - Data controller should collect and process only necessary personal data.",
    "ACCURACY_NAME"                  => "Accuracy of Personal Data",
    "ACCURACY_DESCRIPTION"           => "Article 5.1.d - Data controller should ensure that personal data is accurate and up to date.",
    "STORAGE_LIMITATION_NAME"        => "Storage Limitation",
    "STORAGE_LIMITATION_DESCRIPTION" => "Article 5.1.e - Personal data should be kept in a form that permits identification for no longer than necessary.",
    "CONSENT_NAME"                   => "Consent",
    "CONSENT_DESCRIPTION"            => "Article 7 - The controller must obtain the data subject's freely given, specific, and informed consent for processing their personal data.",
    "ACCESS_NAME"                    => "Access to Personal Data",
    "ACCESS_DESCRIPTION"             => "Article 15 - Data subject can request their personal data.",
    "RECTIFICATION_NAME"             => "Rectification of Personal Data ",
    "RECTIFICATION_DESCRIPTION"      => "Article 16 - Data subject can request correction of inaccurate personal data.",
    "ERASURE_NAME"                   => "Erasure of Personal Data",
    "ERASURE_DESCRIPTION"            => "Article 17 - Data subject can request deletion of their personal data.",
    "RESTRICTION_NAME"               => "Restriction of Processing",
    "RESTRICTION_DESCRIPTION"        => "Article 18 - Data subject can request limitation on the processing of their personal data.",
    "PORTABILITY_NAME"               => "Data Portability",
    "PORTABILITY_DESCRIPTION"        => "Article 20 - Data subject can request to receive their personal data in a machine-readable format.",
    "OBJECTION_NAME"                 => "Objection to Processing",
    "OBJECTION_DESCRIPTION"          => "Article 21 - Data subject can object to the processing of their personal data.",
    "AUTOMATED_NAME"                 => "Automated Decision Making and Profiling",
    "AUTOMATED_DESCRIPTION"          => "Article 22 - Data subject can request explanation or challenge automated decision making.",
];
