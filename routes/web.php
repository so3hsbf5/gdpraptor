<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', [\App\Http\Controllers\AppController::class, 'dashboard'])->name('dashboard');

Route::resources([
    "cases" => \App\Http\Controllers\GdprCaseController::class,
    "requests" => \App\Http\Controllers\GdprRequestController::class,
    "complaints" => \App\Http\Controllers\ComplaintController::class,
]);
