# GDPRaptor

GDPRaptor is an application for managing GDPR requests and complaints. It provides a comprehensive solution to streamline and automate the process of handling GDPR-related issues. This project is built using Laravel 10, making it a robust and scalable solution for managing data protection and privacy compliance.

## Features
### Case
Start your process by creating a new Case. This will store controller details, object and all the details of the requests and complaints.

### Request Management
GDPRaptor allows users to easily manage and track data subject requests, such as access requests, rectification requests, and erasure requests. The application provides a centralized platform to handle these requests efficiently.

### Complaint Tracking
With GDPRaptor, users can keep track of complaints received regarding data protection and privacy. The application provides a structured workflow for managing complaints, ensuring timely resolution and compliance.

### Notifications and Reminders
The application includes notification and reminder functionalities to keep users informed about pending requests, approaching deadlines, and important updates. This feature helps maintain compliance and ensures timely action on GDPR-related matters.


## Installation

To install GDPRaptor, follow these steps:

1. Clone the repository :
```shell
git clone https://github.com/your-username/GDPRaptor.git
```

2. Navigate to the project directory.
3. Install dependencies using Composer:
```shell
composer install
```

4. Copy the example environment file and configure your environment variables (eg : database):
```shell
cp .env.example .env
```
We recommand to use Laravel Sail, in this case, use these credentials for the database :
```
DB_USERNAME=sail
DB_PASSWORD=password
```

5. Generate a new application key:
```shell
php artisan key:generate
```

6. Start the development server:
```shell
./vendor/bin/sail up

# or if you do not use Laravel Sail
php artisan up
```

7. Run database migrations and seed the database:
```
./vendor/bin/sail artisan migrate --seed

# or if you do not use Laravel Sail
php artisan migrate --seed
```

> The app still in pre-development phase. When updating, you should use `php artisan migrate:fresh --seed` to restore the databse and get the last migrations patches.

8. The application will be accessible at http://localhost.

## Contributing

Contributions to GDPRaptor are welcome! If you find any bugs or have suggestions for improvements, please submit an issue or open a pull request on the project's GitHub repository.

Before contributing, please review our contribution guidelines.

## License

GDPRaptor is open-source software licensed under the MIT license. Feel free to use, modify, and distribute the application as per the terms of the license.
