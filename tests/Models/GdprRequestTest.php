<?php

namespace Tests\Models;

use App\Models\GdprRequest;
use Tests\TestCase;

class GdprRequestTest extends TestCase
{

    public function datesProvider(): array
    {
        return [
            "nominal case"           => ["2023-06-06", false, "2023-07-06"],
            "end of month"           => ["2023-10-31", false, "2023-11-30"],
            "end on saturday"        => ["2023-05-10", false, "2023-06-10"],
            "end on sunday"          => ["2023-05-11", false, "2023-06-11"],
            "prolonged nominal case" => ["2023-06-06", true,  "2023-09-06"],
        ];
    }

    /**
     * @dataProvider datesProvider
     */
    public function test_expiringDate(string $createdAt, bool $prolonged, string $expected)
    {
        $request = new GdprRequest([
            "created_at"   => $createdAt,
            "prolonged" => $prolonged,
        ]);

        $this->assertEquals($expected, $request->expiringDate());
    }
}
