@extends('app')

@section('title')
    Requests
@endsection

@push('buttons')
    <a href="{{ route('requests.create') }}" class="btn btn-primary" title="Create new request">New request</a>
@endpush

@push('content')
    <div class="row">
        @if(count($allRequests) > 0)
        <p>This is very empty. You should create a request to begin.</p>
        @endif

        @if(count($runningRequests) > 0)
        <div class="col-6 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Running requests</h4>
                </div>
            </div>
        </div>
        @endif
        @if(count($expiredRequests) > 0)
        <div class="col-6 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Expired</h4>
                </div>
            </div>
        </div>
        @endif

        @if(count($allRequests) > 0)
        <div class="col-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">All Requests</h4>
                </div>
            </div>
        </div>
        @endif
    </div>
@endpush
