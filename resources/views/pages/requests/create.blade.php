@extends('app')

@section('title')
    Requests / New Request
@endsection

@push('content')
    <form action="{{ route('requests.store') }}" method="POST" name="caseCreate" enctype="multipart/form-data">
        @csrf
        <div class="row">
            <div class="col-6 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Case Informations</h4>
                        <div class="form-group">
                            @empty($relatedCase)
                                <label for="case_id">Related Case <span class="text-muted">(required)</span> :</label>
                                <select name="case_id" id="case_id" class="form-control" required>
                                    @foreach(\App\Models\GdprCase::with('company')->get() as $case)
                                        <option value="{{ $case->id }}">#{{ $case->id }} - {{ $case->company->name }}</option>
                                    @endforeach
                                </select>
                            @else
                                <input type="hidden" name="case_id" id="case_id" value="{{ $relatedCase->id }}">
                                <p>Related Case : {{ $relatedCase->company->name }} - {{ $relatedCase->object }}</p>
                            @endempty
                        </div>
                        <div class="form-group">
                            <h4 class="card-title">Objects</h4>
                            @foreach(\App\Domain\GdprObject::cases() as $object)
                                <div class="form-check mb-4">
                                    <label class="form-check-label" for="{{ $object->getName() }}">
                                        <input type="checkbox" name="objects[{{ $object }}]" id="{{ $object->getName() }}" class="form-check-input">
                                        {{ $object->getName() }}
                                    </label>
                                    <i class="input-helper">{{ $object->getDescription() }}</i>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-6 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Case Details</h4>
                        <div class="form-group">
                            <label for="notes">Notes :</label>
                            <textarea class="form-control" name="notes" id="notes" rows="30" placeholder="Received on john.doe@email.com..."></textarea>
                            <small class="text-muted">Supports Markdown</small>
                        </div>
                        <div class="form-group">
                            <input type="submit" value="Create request" class="btn btn-primary">
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </form>
@endpush

@push('scripts')
    <script>
        $('input[name=requestFor]').change(function () {
            $('#otherCompanyWrapper').toggleClass('d-none');
        });
    </script>
@endpush
