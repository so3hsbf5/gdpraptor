@extends('app')

@section('title')
    Cases
@endsection

@push('buttons')
    <a href="{{ route('cases.create') }}" class="btn btn-primary" title="Create new case">New case</a>
@endpush

@push('content')
    <div class="row">
        @if($totalCases === 0)
            <p>This is very empty. You should create a new case to begin.</p>
        @endif

        @if(count($emptyCases) > 0)
        <div class="col-6 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">New cases</h4>
                    <table class="table table-hover" aria-label="New cases">
                        <thead>
                        <tr>
                            <th>Company</th>
                            <th>Object</th>
                            <th>Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($emptyCases as $case)
                            <tr>
                                <td>
                                    <x-company-preview :company="$case->company"/>
                                </td>
                                <td>{{ $case->object }}</td>
                                <td>
                                    <a href="{{ route('cases.show', $case) }}" class="btn btn-sm btn-outline-primary"><span class="material-icons">visibility</span></a>
                                    <a href="{{ route('cases.edit', $case) }}" class="btn btn-sm btn-outline-info"><span class="material-icons">edit</span></a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        @endif
        @if(count($runningCases) > 0)
        <div class="col-6 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Running cases</h4>
                </div>
            </div>
        </div>
        @endif
        @if(count($expiredCases) > 0)
        <div class="col-6 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">With expired requests</h4>
                </div>
            </div>
        </div>
        @endif
            @if(count($otherCases) > 0)
                <div class="col-6 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">Other</h4>
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>Company</th>
                                        <th>Object</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($otherCases as $case)
                                    <tr>
                                        <td><x-company-preview :company="$case->company"/></td>
                                        <td>{{ $case->object }}</td>
                                        <td>
                                            <a href="{{ route('cases.show', $case) }}" class="btn btn-sm btn-outline-primary"><span class="material-icons">visibility</span></a>
                                            <a href="{{ route('cases.edit', $case) }}" class="btn btn-sm btn-outline-info"><span class="material-icons">edit</span></a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            @endif
    </div>
@endpush
