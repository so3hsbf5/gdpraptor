@extends('app')

@section('title')
    Cases / Case #{{ $case->id }} - {{ $case->company->name }}
@endsection

@push('buttons')
    <a href="{{ route('cases.edit', ['case' => $case->id]) }}" class="btn btn-sm btn-info" title="Edit this case"><span class="material-icons">edit</span></a>
    <a href="{{ route('requests.create', ['case' => $case->id]) }}" class="btn btn-primary" title="Create new case">New GDPR Request</a>
    <a href="{{ route('complaints.create', ['case' => $case->id]) }}" class="btn btn-info" title="Create new complaint">New APD Complaint</a>
@endpush

@push('content')
    <div class="row">
        <div class="col-4 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <x-company-preview :company="$case->company"/>

                    <ul class="mt-3">
                        <li><b>Website :</b> {{ $case->company->website }}</li>
                        @foreach($case->company->parameters as $key => $value)
                            <li><b>{{ ucfirst($key) }} :</b> {{ $value }}</li>
                        @endforeach
                    </ul>

                    <p class="mt-3"><b>Object :</b> {{ $case->object }}</p>

                    <img src="{{ asset("storage/" . $case->proof->path) }}" alt="Proof" class="img-fluid" style="max-height: 250px">
                </div>
            </div>
        </div>
        <div class="col-4 grid-margin stretch-card" style="max-height: 70vh; overflow-y: auto;">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Notes</h4>
                    {!! $case->notesFormatted !!}
                </div>
            </div>
        </div>
        <div class="col-4 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Timeline</h4>
                    <x-timeline :transitions="$transitions"/>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-6 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Requests</h4>
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Object</th>
                                <th>State</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($case->gdprRequests as $request)
                            <tr>
                                <td title="{{ $request->objectsAsString }}">{{ \Illuminate\Support\Str::limit($request->objectsAsString, 50) }}</td>
                                <td><x-state-badge :state="$request->state"/></td>
                                <td>
                                    <a href="{{ route('requests.show', $request) }}" class="btn btn-sm btn-outline-primary"><span class="material-icons">visibility</span></a>
                                    <a href="{{ route('requests.edit', $request) }}" class="btn btn-sm btn-outline-info"><span class="material-icons">edit</span></a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-6 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Complaints</h4>
                    <table class="table">
                        <thead>
                        <tr>
                            <th>Object</th>
                            <th>State</th>
                            <th>Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($case->complaints as $complaint)
                            <tr>
                                <td>{{ $complaint->object }}</td>
                                <td><x-state-badge :state="$complaint->state"/></td>
                                <td>
                                    <a href="{{ route('complaints.show', $request) }}" class="btn btn-sm btn-outline-primary"><span class="material-icons">visibility</span></a>
                                    <a href="{{ route('complaints.edit', $request) }}" class="btn btn-sm btn-outline-info"><span class="material-icons">edit</span></a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endpush
