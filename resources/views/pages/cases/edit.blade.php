@extends('app')

@section('title')
    Cases / Edit Case #{{ $case->id }} - {{ $case->company->name }}
@endsection

@push('content')
    <form action="{{ route('cases.update', ['case' => $case->id]) }}" method="POST" name="caseEdit">
        @csrf
        @method('PUT')
        <div class="row">
            <div class="col-8 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Case Informations</h4>
                        <div class="form-group">
                            <label for="object">Object <span class="text-muted">(required)</span> :</label>
                            <input type="text" class="form-control" placeholder="Commercial e-mail received" aria-label="Object"
                                   id="object" name="object" required value="{{ $case->object }}">
                        </div>
                        <div class="form-group">
                            <label for="notes">Notes :</label>
                            <textarea class="form-control" name="notes" id="notes" rows="10" placeholder="Received on john.doe@email.com...">{{ $case->notes }}</textarea>
                            <small class="text-muted">Supports Markdown</small>
                        </div>
                        <div class="form-group">
                            <input type="submit" value="Edit case" class="btn btn-primary">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
@endpush

@push('scripts')
    <script>
        $('#company_email').on( "focusout", function() {
           let domain = $(this).val().split('@')[1];
           $('#company_website').val(domain);
        });
    </script>
@endpush
