@extends('app')

@section('title')
    Cases / New Case
@endsection

@push('content')
    <form action="{{ route('cases.store') }}" method="POST" name="caseCreate" enctype="multipart/form-data">
        @csrf
        <div class="row">
            <div class="col-4 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Company Informations</h4>
                        <div class="form-group">
                            <label for="company_name">Name <span class="text-muted">(required)</span> :</label>
                            <input type="text" class="form-control" placeholder="Evil" aria-label="Name" required
                                   id="company_name" name="company_name">
                        </div>
                        <div class="form-group">
                            <label for="company_email">DPO E-Mail <span class="text-muted">(required)</span> :</label>
                            <input type="email" class="form-control" placeholder="privacy@evil.com" aria-label="E-Mail"
                                   required id="company_email" name="company_email">
                        </div>
                        <div class="form-group">
                            <label for="company_website">Domain Name <span class="text-muted">(required)</span>
                                :</label>
                            <input type="text" class="form-control" placeholder="evil.com" aria-label="URL"
                                   id="company_website" name="company_website" required>
                        </div>
                        <div class="form-group">
                            <label for="company_logo">Logo :</label>
                            <input type="file" class="form-control" id="company_logo" name="company_logo">
                        </div>

                        <div class="form-group">
                            <label for="parameters">Additional Parameters :</label>
                            <textarea name="company_parameters" id="company_parameters" rows="3" class="form-control">phone,111 222 333</textarea>
                            <p class="text-sm text-muted">One pair of (key,value) per row.</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-8 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Case Informations</h4>
                        <div class="form-group">
                            <label for="object">Object <span class="text-muted">(required)</span> :</label>
                            <input type="text" class="form-control" placeholder="Commercial e-mail received" aria-label="Object"
                                   id="object" name="object" required>
                        </div>
                        <div class="form-group">
                            <label for="proof">Proof <span class="text-muted">(required)</span> :</label>
                            <input type="file" class="form-control" name="proof" id="proof" required/>
                        </div>
                        <div class="form-group">
                            <label for="notes">Notes :</label>
                            <textarea class="form-control" name="notes" id="notes" rows="10" placeholder="Received on john.doe@email.com..."></textarea>
                            <small class="text-muted">Supports Markdown</small>
                        </div>
                        <div class="form-group">
                            <input type="submit" value="Create case" class="btn btn-primary">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
@endpush

@push('scripts')
    <script>
        $('#company_email').on( "focusout", function() {
           let domain = $(this).val().split('@')[1];
           $('#company_website').val(domain);
        });
    </script>
@endpush
