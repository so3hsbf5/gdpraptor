<nav class="sidebar sidebar-offcanvas" id="sidebar">
    <ul class="nav">
        <li class="nav-item">
            <a class="nav-link" href="{{ route('dashboard') }}">
                <span class="menu-title">{{ __("Dashboard") }}</span>
                <i class="material-icons menu-icon">dashboard</i>
            </a>
        </li>

        <li class="nav-item">
            <a class="nav-link" href="{{ route('cases.index') }}">
                <span class="menu-title">{{ __("GDPR Cases") }}</span>
                <i class="material-icons menu-icon">folder</i>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="{{ route('requests.index') }}">
                <span class="menu-title">{{ __("GDPR Requests") }}</span>
                <i class="material-icons menu-icon">help</i>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="{{ route('complaints.index') }}">
                <span class="menu-title">{{ __("DPA Complaints") }}</span>
                <i class="material-icons menu-icon">gpp_maybe</i>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="#">
                <span class="menu-title">{{ __("Templates") }}</span>
                <i class="material-icons menu-icon">description</i>
            </a>
        </li>
    </ul>
</nav>
