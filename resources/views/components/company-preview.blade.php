<div class="d-flex px-2 py-1">
    <div>
        <img src="{{ $company->logo }}" class="avatar avatar-sm me-3" alt="{{ $company->name }}'s logo">
    </div>
    <div class="d-flex flex-column justify-content-center">
        <h6 class="mb-0 text-sm">{{ $company->name }}</h6>
        <a href="mailto:{{ $company->email }}" class="text-xs text-secondary mb-0">{{ $company->email }}</a>
    </div>
</div>
