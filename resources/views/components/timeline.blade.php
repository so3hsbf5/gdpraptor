<div class="timeline timeline-one-side" data-timeline-axis-style="dotted">
    @foreach($transitions as $transition)
        <div class="timeline-block mb-3">
            <span class="timeline-step"><span class="material-icons text-{{ $transition->to->color() }}">{{ $transition->icon() }}</span></span>
            <div class="timeline-content">
                <div class="d-flex justify-content-between">
                    <h6 class="badge badge-lg badge-gradient-{{ $transition->to->color() }}">{{ ucfirst(strtolower($transition->to)) }}</h6>
                    <span class="text-secondary text-xs">{{ $transition->created_at }}</span>
                </div>
                <p class="text-sm mt-1">
                    {{ $transition->description }}
                </p>
            </div>
        </div>
    @endforeach
</div>

