<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('gdpr_requests', function (Blueprint $table) {
            $table->id();
            $table->foreignId('gdpr_case_id')->constrained()->cascadeOnDelete();
            $table->string('state');
            $table->json('objects');
            $table->text('notes')->nullable();
            $table->boolean('prolonged')->default(false);

            $table->dateTime('sent_at')->nullable();
            $table->date('expires_at')->nullable();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('gdpr_requests');
    }
};
